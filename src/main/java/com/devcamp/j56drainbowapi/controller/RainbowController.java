package com.devcamp.j56drainbowapi.controller;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j56drainbowapi.service.RainbowService;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping(value="/rainbow-request-query")
    public ArrayList<String> filterRainbow(@RequestParam(value="q", defaultValue = "") String keyword) {

        //RainbowService rainbowService = new RainbowService();
        return rainbowService.filterRainbow(keyword);
    }

    @GetMapping("/rainbow-request-param/{ind}")
    public String getRainbow(@PathVariable(name="ind") int index) {
        return rainbowService.getRainbow(index);
    }
    
}
