package com.devcamp.j56drainbowapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public ArrayList<String> filterRainbow(String keyword)  {
        ArrayList<String> listRainbow = new ArrayList<>();

        for (String rainbow : this.rainbows) {
            if (rainbow.contains(keyword)) {
                listRainbow.add(rainbow);
            }
        }
        return listRainbow;
    }

    public String getRainbow(int index) {
        String rainbow = "";

        if (index >=0 && index <= 6) {
            rainbow = this.rainbows[index];
        }
        return rainbow;
    }
}
