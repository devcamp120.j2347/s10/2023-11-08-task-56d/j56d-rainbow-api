package com.devcamp.j56drainbowapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J56dRainbowApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(J56dRainbowApiApplication.class, args);
	}

}
